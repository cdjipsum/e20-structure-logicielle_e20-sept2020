let mesEtudiants = {'date':[''], 'nom':[''], 'prenom':[''], 'email':[''], 'phone':['']};
let mesEntreprises = {'date':[''], 'nom':[''], 'prenom':[''], 'email':[''], 'phone':['']};

function adminFunction() {
    let html = '';
    html += '<div id="inputsAdmin"><div>Ajouter <input type="radio" id="ajout"></div>';
    html += '<div>Modifier <input type="radio" id="modifie"></div>';
    html += '<div>Supprimer <input type="radio" id="supprim"></div>';
	html += '<div>Filtrer <input type="radio" id="filtre"></div></div>';
    return html;
}

function adminFunction1() {
    let html = '';
    html += '<div id="inputsAdmin1"><div>Etudiant <input type="radio" id="etud"></div>';
    html += '<div>Entreprise <input type="radio" id="entrep"></div></div>';
    return html;
}

function check1() {
    if (document.getElementById("ajout").checked == true) {
        document.getElementById("modifie").checked = false;
        document.getElementById("supprim").checked = false;
		document.getElementById("filtre").checked = false;
    }
	check6();
}

function check2() {
    if (document.getElementById("modifie").checked == true) {
        document.getElementById("ajout").checked = false;
        document.getElementById("supprim").checked = false;
		document.getElementById("filtre").checked = false;
    }
	check6();
}

function check3() {
    if (document.getElementById("supprim").checked == true) {
        document.getElementById("ajout").checked = false;
        document.getElementById("modifie").checked = false;
		document.getElementById("filtre").checked = false;
    }
	check6();
}

function check7() {
    if (document.getElementById("filtre").checked == true) {
        document.getElementById("ajout").checked = false;
        document.getElementById("modifie").checked = false;
		document.getElementById("supprim").checked = false;
    }
	check6();
}

function check4() {
    if (document.getElementById("etud").checked == true) {
        document.getElementById("entrep").checked = false;
    }
	check6();
}

function check5() {
    if (document.getElementById("entrep").checked == true) {
        document.getElementById("etud").checked = false;
    }
	check6();
}

function check6() {
    if (document.getElementById("ajout").checked && document.getElementById("etud").checked) {
		document.getElementById("myForm").style.visibility = 'visible';
        document.getElementById("workAdmin").innerHTML = affichage1().toString();
    }
    if (document.getElementById("ajout").checked && document.getElementById("entrep").checked) {
		document.getElementById("myForm").style.visibility = 'visible';
        document.getElementById("workAdmin").innerHTML = affichage2().toString();
    }
    if (document.getElementById("modifie").checked && document.getElementById("etud").checked) {
		document.getElementById("myForm").style.visibility = 'visible';
        document.getElementById("workAdmin").innerHTML = affichage1().toString();
    }
    if (document.getElementById("modifie").checked && document.getElementById("entrep").checked) {
		document.getElementById("myForm").style.visibility = 'visible';
        document.getElementById("workAdmin").innerHTML = affichage2().toString();
    }
    if (document.getElementById("supprim").checked && document.getElementById("etud").checked) {
		document.getElementById("myForm").style.visibility = 'visible';
        document.getElementById("workAdmin").innerHTML = affichage1().toString();
    }
    if (document.getElementById("supprim").checked && document.getElementById("entrep").checked) {
		document.getElementById("myForm").style.visibility = 'visible';
        document.getElementById("workAdmin").innerHTML = affichage2().toString();
    }
	if (document.getElementById("filtre").checked && document.getElementById("etud").checked) {
		document.getElementById("myForm").style.visibility = 'visible';
        document.getElementById("workAdmin").innerHTML = affichage3().toString();
    }
	if (document.getElementById("filtre").checked && document.getElementById("entrep").checked) {
		document.getElementById("myForm").style.visibility = 'visible';
        document.getElementById("workAdmin").innerHTML = affichage3().toString();
    }
}

function affichage1() {
	let html = '';
    html += '<div>Date <input type="date" id="date"></div>';
	html += '<div>Nom <input type="text" id="nom"></div>';
	html += '<div>Prenom <input type="text" id="prenom"></div>';
	html += '<div>Email <input type="email" id="email"></div>';
    html += '<div>Telephone <input type="tel" id="phone" placeholder="123-456-789" maxlength="11"></div>';
    return html;
}

function affichage2() {
	let html = '';
    html += '<div>Date <input type="date" id="date"></div>';
	html += '<div>Nom de ton entreprise <input type="text" id="nom"></div>';
	html += '<div>Nom du contact <input type="text" id="prenom"></div>';
	html += '<div>Email <input type="email" id="email"></div>';
    html += '<div>Telephone <input type="tel" id="phone" placeholder="123-456-789" maxlength="11"></div>';
    return html;
}

function affichage3() {
	let html = '';
    /*html += '<div>Selon id <input type="text" id="myId"></div>';*/
	html += '<div>Selon le nom <input type="text" id="nom"></div>';
    return html;
}

function makeTable(infos) {
	let html = '';
	let copyInfos = infos;
	let verify = false;
	if (copyInfos == 'ajoutEt' || copyInfos == 'ajoutEn' || copyInfos == 'modifiEt' || copyInfos == 'modifiEn'
		|| copyInfos == 'supprimEn' || copyInfos == 'supprimEt') {
		if (infos == 'ajoutEt' || infos == 'modifiEt' || copyInfos == 'supprimEt') {
			infos = mesEtudiants;
		}
		if (infos == 'ajoutEn' || infos == 'modifiEn' || copyInfos == 'supprimEn') {
			infos = mesEntreprises;
		}
		for (let i = 0; i < infos.nom.length; i++) {
			if (copyInfos == 'ajoutEt' || copyInfos == 'ajoutEn') {
				if (document.getElementById("nom").value != '' && document.getElementById("prenom").value != ''
				&& document.getElementById("date").value != '' && document.getElementById("email").value != ''
				&& document.getElementById("phone").value != ''
				&& infos.prenom[i] != document.getElementById("prenom").value && i == infos.nom.length - 1) {
					infos.date.push(document.getElementById("date").value);
					infos.nom.push(document.getElementById("nom").value);
					infos.prenom.push(document.getElementById("prenom").value);
					infos.email.push(document.getElementById("email").value);
					infos.phone.push(document.getElementById("phone").value);
					verify = true;
				}
			}
			if (copyInfos == 'modifiEt' || copyInfos == 'modifiEn') {
				if (infos.nom[i] == document.getElementById("nom").value) {
					if (document.getElementById("date").value != '') {
						infos.date[i] = document.getElementById("date").value;
					}
					if (document.getElementById("prenom").value != '') {
						infos.prenom[i] = document.getElementById("prenom").value;
					}
					if (document.getElementById("email").value != '') {
						infos.email[i] = document.getElementById("email").value;
					}
					if (document.getElementById("phone").value != '') {
						infos.phone[i] = document.getElementById("phone").value;
					}
					verify = true;
				}
			}
			if (copyInfos == 'supprimEt' || copyInfos == 'supprimEn') {
				if (infos.nom[i] == document.getElementById("nom").value) {
					infos.nom[i] = '';
					infos.prenom[i] = '';
					infos.email[i] = '';
					infos.phone[i] = '';
					infos.date[i] = '';
					verify = true;
				}
			}
		}
	}
	if (copyInfos == 'filtreNomEtu' || copyInfos == 'filtreNomEn') {
		verify = true;
	}
	if (verify) {
		if (copyInfos == 'filtreNomEtu' || copyInfos == 'modifiEt' || copyInfos == 'ajoutEt' || copyInfos == 'supprimEt') {
			infos = mesEtudiants;
		}
		if (copyInfos == 'filtreNomEn' || copyInfos == 'modifiEn' || copyInfos == 'ajoutEn' || copyInfos == 'supprimEn') {
			infos = mesEntreprises;
		}
		html += '<table><tr><th>Date</th><th>Nom</th><th>Prenom</th><th>Email</th><th>Phone</th></tr>';
		for (let x = 0; x < infos.nom.length; x++) {
			if (infos.nom[x] != '' && (copyInfos != 'filtreIdEtu' && copyInfos != 'filtreIdEn' && copyInfos != 'filtreNomEtu' && 
				copyInfos != 'filtreNomEn')) {
				html += '<tr><td>' + infos.date[x] + '</td><td>' + infos.nom[x] + '</td>';
				html += '<td>' + infos.prenom[x] + '</td><td>' + infos.email[x] + '</td>';
				html += '<td>' + infos.phone[x] + '</td></tr>';
			}
			if (document.getElementById("nom").value != "" && infos.nom[x] == document.getElementById("nom").value &&
				(copyInfos == 'filtreIdEtu' || copyInfos == 'filtreIdEn' || copyInfos == 'filtreNomEtu' || 
				copyInfos == 'filtreNomEn')) {
				html += '<tr><td>' + infos.date[x] + '</td><td>' + infos.nom[x] + '</td>';
				html += '<td>' + infos.prenom[x] + '</td><td>' + infos.email[x] + '</td>';
				html += '<td>' + infos.phone[x] + '</td></tr>';
			}
		}
		html += '</table>';
	}
	return html;
}

function myTable(operation) {
	let html = '';
	html += makeTable(operation);
	return html;
}

function results() {
	if (document.getElementById("ajout").checked && document.getElementById("etud").checked) {
		document.getElementById("result").innerHTML = "Etudiant " + document.getElementById("nom").value + " a ete ajoute dans la bd";
		document.getElementById("lister").innerHTML = myTable('ajoutEt').toString();
		if (document.getElementById("nom").value == "" || document.getElementById("prenom").value == "") {
			document.getElementById("result").innerHTML = "";
			document.getElementById("lister").innerHTML = "";
		}
	}
	if (document.getElementById("ajout").checked && document.getElementById("entrep").checked) {
		document.getElementById("result").innerHTML = "Entreprise " + document.getElementById("nom").value + " a ete ajoute dans la bd";
		document.getElementById("lister").innerHTML = myTable('ajoutEn').toString();
		if (document.getElementById("nom").value == "" || document.getElementById("prenom").value == "") {
			document.getElementById("result").innerHTML = "";
			document.getElementById("lister").innerHTML = "";
		}
	}
	if (document.getElementById("modifie").checked && document.getElementById("etud").checked) {
		document.getElementById("result").innerHTML = "Etudiant " + document.getElementById("nom").value + " a ete modifie dans la bd";
		document.getElementById("lister").innerHTML = myTable('modifiEt').toString();
		if (document.getElementById("nom").value == "" || document.getElementById("prenom").value == "") {
			document.getElementById("result").innerHTML = "";
			document.getElementById("lister").innerHTML = "";
		}
	}
	if (document.getElementById("modifie").checked && document.getElementById("entrep").checked) {
		document.getElementById("result").innerHTML = "Entreprise " + document.getElementById("nom").value + " a ete modifie dans la bd";
		document.getElementById("lister").innerHTML = myTable('modifiEn').toString();
		if (document.getElementById("nom").value == "" || document.getElementById("prenom").value == "") {
			document.getElementById("result").innerHTML = "";
			document.getElementById("lister").innerHTML = "";
		}
	}
	if (document.getElementById("supprim").checked && document.getElementById("etud").checked) {
		document.getElementById("result").innerHTML = "Etudiant " + document.getElementById("nom").value + " est supprime de la bd";
		document.getElementById("lister").innerHTML = myTable('supprimEt').toString();
		if (document.getElementById("nom").value == "" || document.getElementById("prenom").value == "") {
			document.getElementById("result").innerHTML = "";
			document.getElementById("lister").innerHTML = "";
		}
	}
	if (document.getElementById("supprim").checked && document.getElementById("entrep").checked) {
		document.getElementById("result").innerHTML = "Entreprise " + document.getElementById("nom").value + " est supprime de la bd";
		document.getElementById("lister").innerHTML = myTable('supprimEn').toString();
		if (document.getElementById("nom").value == "" || document.getElementById("prenom").value == "") {
			document.getElementById("result").innerHTML = "";
			document.getElementById("lister").innerHTML = "";
		}
	}
	if (document.getElementById("filtre").checked && document.getElementById("etud").checked) {
		/*if (document.getElementById("myId").value != "") {
			document.getElementById("result").innerHTML = "Resultat du filtrage des etudiants avec comme id " + document.getElementById("myId").value;
			document.getElementById("lister").innerHTML = myTable('filtreIdEtu').toString();
		}*/
		if (document.getElementById("nom").value != "") {
			document.getElementById("result").innerHTML = "Resultat du filtrage des etudiants avec comme nom " + document.getElementById("nom").value;
			document.getElementById("lister").innerHTML = myTable('filtreNomEtu').toString();
		}
		/*if (document.getElementById("myId").value == "" && document.getElementById("nom").value == "") {
			document.getElementById("result").innerHTML = "";
			document.getElementById("lister").innerHTML = "";
		}*/
		if (document.getElementById("nom").value == "") {
			document.getElementById("result").innerHTML = "";
			document.getElementById("lister").innerHTML = "";
		}
	}
    if (document.getElementById("filtre").checked && document.getElementById("entrep").checked) {
        /*if (document.getElementById("myId").value != "") {
			document.getElementById("result").innerHTML = "Resultat du filtrage des entreprises avec comme id " + document.getElementById("myId").value;
			document.getElementById("lister").innerHTML = myTable('filtreIdEn').toString();
		}*/
		if (document.getElementById("nom").value != "") {
			document.getElementById("result").innerHTML = "Resultat du filtrage des entreprises avec comme nom " + document.getElementById("nom").value;
			document.getElementById("lister").innerHTML = myTable('filtreNomEn').toString();
		}
		/*if (document.getElementById("myId").value == "" && document.getElementById("nom").value == "") {
			document.getElementById("result").innerHTML = "";
			document.getElementById("lister").innerHTML = "";
		}*/
		if (document.getElementById("nom").value == "") {
			document.getElementById("result").innerHTML = "";
			document.getElementById("lister").innerHTML = "";
		}
	}
}

document.getElementById("superAdmin").innerHTML = adminFunction().toString();
document.getElementById("superAdmin1").innerHTML = adminFunction1().toString();
document.getElementById("ajout").addEventListener("change", check1);
document.getElementById("modifie").addEventListener("change", check2);
document.getElementById("supprim").addEventListener("change", check3);
document.getElementById("filtre").addEventListener("change", check7);
document.getElementById("etud").addEventListener("change", check4);
document.getElementById("entrep").addEventListener("change", check5);
/*document.getElementById("workAdmin").addEventListener("click", check6);*/