let mesSuivis = {'date':[''], 'titre':[''], 'desc':['']};
let mesStages = {'date':[''], 'titre':[''], 'desc':[''], 'langue':[''], 'numR':[''], 'renum':['']};

function secretaireFunction() {
    let html = '';
    html += '<div id="inputsSecretaire"><div>Ajouter <input type="radio" id="ajout"></div>';
    html += '<div>Modifier <input type="radio" id="modifie"></div>';
    html += '<div>Supprimer <input type="radio" id="supprim"></div>';
	html += '<div>Filtrer <input type="radio" id="filtre"></div></div>';
    return html;
}

function secretaireFunction1() {
    let html = '';
    html += '<div id="inputsSecretaire1"><div>Suivi de stage <input type="radio" id="suivi"></div>';
	html += '<div>Stage <input type="radio" id="stage"></div></div>';
    return html;
}

function check1() {
    if (document.getElementById("ajout").checked == true) {
        document.getElementById("modifie").checked = false;
        document.getElementById("supprim").checked = false;
		document.getElementById("filtre").checked = false;
    }
	check4();
}

function check2() {
    if (document.getElementById("modifie").checked == true) {
        document.getElementById("ajout").checked = false;
        document.getElementById("supprim").checked = false;
		document.getElementById("filtre").checked = false;
    }
	check4();
}

function check3() {
    if (document.getElementById("supprim").checked == true) {
        document.getElementById("ajout").checked = false;
        document.getElementById("modifie").checked = false;
		document.getElementById("filtre").checked = false;
    }
	check4();
}

function check5() {
    if (document.getElementById("filtre").checked == true) {
        document.getElementById("ajout").checked = false;
        document.getElementById("modifie").checked = false;
		document.getElementById("supprim").checked = false;
    }
	check4();
}

function check6() {
    if (document.getElementById("suivi").checked == true) {
        document.getElementById("stage").checked = false;
    }
	check4();
}

function check7() {
    if (document.getElementById("stage").checked == true) {
        document.getElementById("suivi").checked = false;
    }
	check4();
}

function check4() {
    if (document.getElementById("ajout").checked && document.getElementById("stage").checked) {
		document.getElementById("myForm").style.visibility = 'visible';
        document.getElementById("workSecretaire").innerHTML = affichage3().toString();
    }
    if (document.getElementById("modifie").checked && document.getElementById("stage").checked) {
		document.getElementById("myForm").style.visibility = 'visible';
        document.getElementById("workSecretaire").innerHTML = affichage3().toString();
    }
    if (document.getElementById("supprim").checked && document.getElementById("stage").checked) {
		document.getElementById("myForm").style.visibility = 'visible';
        document.getElementById("workSecretaire").innerHTML = affichage3().toString();
    }
	if (document.getElementById("ajout").checked && document.getElementById("suivi").checked) {
		document.getElementById("myForm").style.visibility = 'visible';
        document.getElementById("workSecretaire").innerHTML = affichage4().toString();
    }
    if (document.getElementById("modifie").checked && document.getElementById("suivi").checked) {
		document.getElementById("myForm").style.visibility = 'visible';
        document.getElementById("workSecretaire").innerHTML = affichage4().toString();
    }
    if (document.getElementById("supprim").checked && document.getElementById("suivi").checked) {
		document.getElementById("myForm").style.visibility = 'visible';
        document.getElementById("workSecretaire").innerHTML = affichage4().toString();
    }
	if (document.getElementById("filtre").checked && document.getElementById("suivi").checked) {
		document.getElementById("myForm").style.visibility = 'visible';
        document.getElementById("workSecretaire").innerHTML = affichage5().toString();
    }
	if (document.getElementById("filtre").checked && document.getElementById("stage").checked) {
		document.getElementById("myForm").style.visibility = 'visible';
        document.getElementById("workSecretaire").innerHTML = affichage5().toString();
    }
}

function affichage3() {
	let html = '';
    html += '<div>Date fin affichage <input type="date" id="date"></div>';
	html += '<div>Titre du poste <input type="text" id="titre"></div>';
	html += '<div>Description du poste <input type="text" id="desc"></div>';
	html += '<div>Language <input type="text" id="langue"></div>';
	html += '<div>Numero de reference <input type="text" id="numR"></div>';
	html += '<div>Renumeration <input type="text" id="renum"></div>';
    return html;
}

function affichage4() {
	let html = '';
    html += '<div>Date <input type="date" id="date"></div>';
	html += '<div>Titre de suivi <input type="text" id="titre"></div>';
	html += '<div>Description de suivi <input type="text" id="desc"></div>';
    return html;
}

function affichage5() {
	let html = '';
    /*html += '<div>Selon id <input type="text" id="myId"></div>';*/
	html += '<div>Selon le titre <input type="text" id="titre"></div>';
    return html;
}

function makeTable(infos) {
	let html = '';
	let copyInfos = infos;
	let verify = false;
	if (copyInfos == 'ajoutSuivi' || copyInfos == 'ajoutStage' || copyInfos == 'modifiSuivi' || copyInfos == 'modifiStage'
		|| copyInfos == 'supprimStage' || copyInfos == 'supprimSuivi') {
		if (infos == 'ajoutSuivi' || infos == 'modifiSuivi' || copyInfos == 'supprimSuivi') {
			infos = mesSuivis;
		}
		if (infos == 'ajoutStage' || infos == 'modifiStage' || copyInfos == 'supprimStage') {
			infos = mesStages;
		}
		for (let i = 0; i < infos.titre.length; i++) {
			if (copyInfos == 'ajoutSuivi' || copyInfos == 'ajoutStage') {
				if (copyInfos == 'ajoutSuivi') {
					if (document.getElementById("titre").value != '' && document.getElementById("desc").value != ''
					&& document.getElementById("date").value != ''
					&& infos.desc[i] != document.getElementById("desc").value && i == infos.titre.length - 1) {
						infos.date.push(document.getElementById("date").value);
						infos.titre.push(document.getElementById("titre").value);
						infos.desc.push(document.getElementById("desc").value);
						if (copyInfos == 'ajoutStage') {
							infos.numR.push(document.getElementById("numR").value);
							infos.renum.push(document.getElementById("renum").value);
							infos.langue.push(document.getElementById("langue").value);
						}
						verify = true;
					}
				}
				if (copyInfos == 'ajoutStage') {
					if (document.getElementById("titre").value != '' && document.getElementById("desc").value != ''
					&& document.getElementById("date").value != '' && document.getElementById("numR").value != '' &&
					document.getElementById("renum").value != '' && document.getElementById("langue").value != ''
					&& infos.desc[i] != document.getElementById("desc").value && i == infos.titre.length - 1) {
						infos.date.push(document.getElementById("date").value);
						infos.titre.push(document.getElementById("titre").value);
						infos.desc.push(document.getElementById("desc").value);
						if (copyInfos == 'ajoutStage') {
							infos.numR.push(document.getElementById("numR").value);
							infos.renum.push(document.getElementById("renum").value);
							infos.langue.push(document.getElementById("langue").value);
						}
						verify = true;
					}
				}
			}
			if (copyInfos == 'modifiSuivi' || copyInfos == 'modifiStage') {
				if (infos.titre[i] == document.getElementById("titre").value) {
					if (copyInfos == 'modifiSuivi') {
						if (document.getElementById("date").value != '') {
							infos.date[i] = document.getElementById("date").value;
						}
						if (document.getElementById("desc").value != '') {
							infos.desc[i] = document.getElementById("desc").value;
						}
						verify = true;
					}
					if (copyInfos == 'modifiStage') {
						if (document.getElementById("date").value != '') {
							infos.date[i] = document.getElementById("date").value;
						}
						if (document.getElementById("desc").value != '') {
							infos.desc[i] = document.getElementById("desc").value;
						}
						if (document.getElementById("numR").value != '') {
							infos.numR[i] = document.getElementById("numR").value;
						}
						if (document.getElementById("renum").value != '') {
							infos.renum[i] = document.getElementById("renum").value;
						}
						if (document.getElementById("langue").value != '') {
							infos.langue[i] = document.getElementById("langue").value;
						}
						verify = true;
					}
				}
			}
			if (copyInfos == 'supprimStage' || copyInfos == 'supprimSuivi') {
				if (infos.titre[i] == document.getElementById("titre").value && copyInfos == 'supprimSuivi') {
					infos.titre[i] = '';
					infos.desc[i] = '';
					infos.date[i] = '';
					verify = true;
				}
				if (infos.titre[i] == document.getElementById("titre").value && copyInfos == 'supprimStage') {
					infos.titre[i] = '';
					infos.desc[i] = '';
					infos.date[i] = '';
					infos.renum[i] = '';
					infos.langue[i] = '';
					infos.numR[i] = '';
					verify = true;
				}
			}
		}
	}
	if (copyInfos == 'filtreNomSuivi' || copyInfos == 'filtreNomStage') {
		verify = true;
	}
	if (verify) {
		html += '<table>';
		if (copyInfos == 'filtreNomSuivi' || copyInfos == 'ajoutSuivi' || copyInfos == 'modifiSuivi' || copyInfos == 'supprimSuivi') {
			infos = mesSuivis;
			html += '<tr><th>Date</th><th>Titre</th><th>Description</th></tr>';
		}
		if (copyInfos == 'filtreNomStage' || copyInfos == 'ajoutStage' || copyInfos == 'modifiStage'|| copyInfos == 'supprimStage') {
			infos = mesStages;
			html += '<tr><th>Date</th><th>Titre</th><th>Description</th><th>Langue</th><th>Numero reference</th><th>Renumeration</th></tr>';
		}
		for (let x = 0; x < infos.titre.length; x++) {
			if (infos.titre[x] != '' && (copyInfos != 'filtreNomSuivi' && copyInfos != 'filtreNomStage')) {
				if (copyInfos == 'ajoutSuivi' || copyInfos == 'modifiSuivi' || copyInfos == 'supprimSuivi') {
					html += '<tr><td>' + infos.date[x] + '</td><td>' + infos.titre[x] + '</td>';
					html += '<td>' + infos.desc[x] + '</td></tr>';
				}
				if (copyInfos == 'ajoutStage' || copyInfos == 'modifiStage' || copyInfos == 'supprimStage') {
					html += '<tr><td>' + infos.date[x] + '</td><td>' + infos.titre[x] + '</td>';
					html += '<td>' + infos.desc[x] + '</td><td>' + infos.langue[x] + '</td>';
					html += '<td>' + infos.numR[x] + '</td><td>' + infos.renum[x] + '</td></tr>';
				}
			}
			if (document.getElementById("titre").value != "" && infos.titre[x] == document.getElementById("titre").value &&
				(copyInfos == 'filtreNomSuivi' || copyInfos == 'filtreNomStage')) {
				if (copyInfos == 'filtreNomSuivi') {
					html += '<tr><td>' + infos.date[x] + '</td><td>' + infos.titre[x] + '</td>';
					html += '<td>' + infos.desc[x] + '</td></tr>';
				}
				if (copyInfos == 'filtreNomStage') {
					html += '<tr><td>' + infos.date[x] + '</td><td>' + infos.titre[x] + '</td>';
					html += '<td>' + infos.desc[x] + '</td><td>' + infos.langue[x] + '</td>';
					html += '<td>' + infos.numR[x] + '</td><td>' + infos.renum[x] + '</td></tr>';
				}
			}
		}
		html += '</table>';
	}
	return html;
}

function myTable(operation) {
	let html = '';
	html += makeTable(operation);
	return html;
}

function results() {
	if (document.getElementById("ajout").checked && document.getElementById("suivi").checked) {
        document.getElementById("result").innerHTML = "Suivi de stage " + document.getElementById("titre").value + " a ete ajoute dans la bd";
		document.getElementById("lister").innerHTML = myTable('ajoutSuivi').toString();
		if (document.getElementById("titre").value == "") {
			document.getElementById("result").innerHTML = "";
			document.getElementById("lister").innerHTML = "";
		}
    }
    if (document.getElementById("modifie").checked && document.getElementById("suivi").checked) {
        document.getElementById("result").innerHTML = "Suivi de stage " + document.getElementById("titre").value + " a ete modifie dans la bd";
		document.getElementById("lister").innerHTML = myTable('modifiSuivi').toString();
		if (document.getElementById("titre").value == "") {
			document.getElementById("result").innerHTML = "";
			document.getElementById("lister").innerHTML = "";
		}
    }
    if (document.getElementById("supprim").checked && document.getElementById("suivi").checked) {
        document.getElementById("result").innerHTML = "Suivi de stage " + document.getElementById("titre").value + " est supprime de la bd";
		document.getElementById("lister").innerHTML = myTable('supprimSuivi').toString();
		if (document.getElementById("titre").value == "") {
			document.getElementById("result").innerHTML = "";
			document.getElementById("lister").innerHTML = "";
		}
    }
	if (document.getElementById("ajout").checked && document.getElementById("stage").checked) {
        document.getElementById("result").innerHTML = "Stage " + document.getElementById("titre").value + " a ete ajoute dans la bd";
		document.getElementById("lister").innerHTML = myTable('ajoutStage').toString();
		if (document.getElementById("titre").value == "") {
			document.getElementById("result").innerHTML = "";
			document.getElementById("lister").innerHTML = "";
		}
    }
    if (document.getElementById("modifie").checked && document.getElementById("stage").checked) {
        document.getElementById("result").innerHTML = "Stage " + document.getElementById("titre").value + " a ete modifie dans la bd";
		document.getElementById("lister").innerHTML = myTable('modifiStage').toString();
		if (document.getElementById("titre").value == "") {
			document.getElementById("result").innerHTML = "";
			document.getElementById("lister").innerHTML = "";
		}
    }
    if (document.getElementById("supprim").checked && document.getElementById("stage").checked) {
        document.getElementById("result").innerHTML = "Stage " + document.getElementById("titre").value + " est supprime de la bd";
		document.getElementById("lister").innerHTML = myTable('supprimStage').toString();
		if (document.getElementById("titre").value == "") {
			document.getElementById("result").innerHTML = "";
			document.getElementById("lister").innerHTML = "";
		}
    }
	if (document.getElementById("filtre").checked && document.getElementById("suivi").checked) {
		/*if (document.getElementById("myId").value != "") {
			document.getElementById("result").innerHTML = "Resultat du filtrage des suivis de stage avec comme id " + document.getElementById("myId").value;
			document.getElementById("lister").innerHTML = "Liste des suivis de stage";
		}*/
		if (document.getElementById("titre").value != "") {
			document.getElementById("result").innerHTML = "Resultat du filtrage des suivis de stage avec comme titre " + document.getElementById("titre").value;
			document.getElementById("lister").innerHTML = myTable('filtreNomSuivi').toString();
		}
		/*if (document.getElementById("myId").value == "" && document.getElementById("titre").value == "") {
			document.getElementById("result").innerHTML = "";
			document.getElementById("lister").innerHTML = "";
		}*/
		if (document.getElementById("titre").value == "") {
			document.getElementById("result").innerHTML = "";
			document.getElementById("lister").innerHTML = "";
		}
	}
    if (document.getElementById("filtre").checked && document.getElementById("stage").checked) {
        /*if (document.getElementById("myId").value != "") {
			document.getElementById("result").innerHTML = "Resultat du filtrage des stages avec comme id " + document.getElementById("myId").value;
			document.getElementById("lister").innerHTML = "Liste des stages";
		}*/
		if (document.getElementById("titre").value != "") {
			document.getElementById("result").innerHTML = "Resultat du filtrage des stages avec comme titre " + document.getElementById("titre").value;
			document.getElementById("lister").innerHTML = myTable('filtreNomStage').toString();
		}
		/*if (document.getElementById("myId").value == "" && document.getElementById("titre").value == "") {
			document.getElementById("result").innerHTML = "";
			document.getElementById("lister").innerHTML = "";
		}*/
		if (document.getElementById("titre").value == "") {
			document.getElementById("result").innerHTML = "";
			document.getElementById("lister").innerHTML = "";
		}
	}
}

document.getElementById("secretaire").innerHTML = secretaireFunction().toString();
document.getElementById("secretaire1").innerHTML = secretaireFunction1().toString();
document.getElementById("ajout").addEventListener("change", check1);
document.getElementById("modifie").addEventListener("change", check2);
document.getElementById("supprim").addEventListener("change", check3);
document.getElementById("filtre").addEventListener("change", check5);
document.getElementById("suivi").addEventListener("change", check6);
document.getElementById("stage").addEventListener("change", check7);
/*document.getElementById("workSecretaire").addEventListener("click", check4);*/