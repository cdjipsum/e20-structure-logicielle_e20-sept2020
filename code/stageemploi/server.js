'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const nodeFs = require('../05-node/exercice/node-fs')
const { Router } = require('express')
const app = express()
const fs = require('fs')

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse application/json
app.use(bodyParser.json())

const PORT = 8080
const HTTP_OK = 200
const CONTENT_TYPE_JSON = 'application/json'
const CONTENT_TYPE_HTML = 'text/html'

app.get('/', function (request, response) {
    response.send("<a href='login.html'>Login page</a>");
})

app.get('/login.html', function (request, response) {
    fs.readFile('login.html', function(err, datas) {
        response.writeHead(HTTP_OK, {'Content-Type': CONTENT_TYPE_HTML});
        response.write(datas);
        return response.end();
    });
})

app.get('/login.jsp', function (request, response) {
    fs.readFile('login.jsp', function(err, datas) {
        response.writeHead(HTTP_OK, {'Content-Type': CONTENT_TYPE_HTML});
        response.write(datas);
        return response.end();
    });
})

app.get('/superAdminPage.html', function (request, response) {
    fs.readFile('superAdminPage.html', function(err, datas) {
        response.writeHead(HTTP_OK, {'Content-Type': CONTENT_TYPE_HTML});
        response.write(datas);
        return response.end();
    });
})

app.get('/superAdminPage.jsp', function (request, response) {
    fs.readFile('superAdminPage.jsp', function(err, datas) {
        response.writeHead(HTTP_OK, {'Content-Type': CONTENT_TYPE_HTML});
        response.write(datas);
        return response.end();
    });
})

app.get('/loginInfoAdmin.jsp', function (request, response) {
    fs.readFile('loginInfoAdmin.jsp', function(err, datas) {
        response.writeHead(HTTP_OK, {'Content-Type': CONTENT_TYPE_HTML});
        response.write(datas);
        return response.end();
    });
})

app.get('/secretairePage.html', function (request, response) {
    fs.readFile('secretairePage.html', function(err, datas) {
        response.writeHead(HTTP_OK, {'Content-Type': CONTENT_TYPE_HTML});
        response.write(datas);
        return response.end();
    });
})

app.get('/secretairePage.jsp', function (request, response) {
    fs.readFile('secretairePage.jsp', function(err, datas) {
        response.writeHead(HTTP_OK, {'Content-Type': CONTENT_TYPE_HTML});
        response.write(datas);
        return response.end();
    });
})

app.get('/loginInfoSecretaire.jsp', function (request, response) {
    fs.readFile('loginInfoSecretaire.jsp', function(err, datas) {
        response.writeHead(HTTP_OK, {'Content-Type': CONTENT_TYPE_HTML});
        response.write(datas);
        return response.end();
    });
})

let http = require('http')
const mariadb = require('mariadb');
const pool = mariadb.createPool({
    host: 'localhost',
    port: 3310,
    user:'root',
    password: 'rootroot',
    database: 'stageemplois'
});

// serveur pour recuperer les infos de la mariadb
let server = http.createServer(((req, res) => {
    res.writeHead(200, {'Content-Type' : 'text/html charset=utf-8'})

	pool.getConnection()
		.then((conn) => {
            conn.query("select * from users")
			.then((rows) => {
                res.write('Login names for connection')
				res.write('<ul>')
				for(let row of rows) {
					res.write('<li>' + row.nom + '</li>')
				}
                res.write('</ul>')
                res.write('<a href="http://localhost:8080/">Employment page</a>')
				res.end()
            })
		}).catch(err => {
    })
}))
server.listen(3000)

app.listen(PORT, function () {
    console.log('Server listening on: http://localhost:%s', PORT)
})
