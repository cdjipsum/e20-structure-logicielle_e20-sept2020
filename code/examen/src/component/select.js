import React from 'react'

// function renderOption (option, index, onChange) {
//     return <option value={option.city} key={index} onChange={onChange}>{option.city}</option>
// }

const Select = ({ id, label, options, onChange }) => (
    <div>
        <label htmlFor={id}>{label}</label>
        <select className='custom-select' id={id}>
            {options.map((option, index) => <option value={option.city} key={index} onChange={onChange}>{option.city}</option>)}
        </select>
    </div>
)

export default Select
