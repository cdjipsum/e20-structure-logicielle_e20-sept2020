import React from 'react'

const Input = ({ id, label, type, onChange }) => (
    <div>
        <label htmlFor={id}>{label}</label>
        <input type={type} name={id} id={id} onChange={onChange} />
    </div>
)

export default Input
