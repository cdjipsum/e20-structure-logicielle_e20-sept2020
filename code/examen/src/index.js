import React from 'react'
import ReactDOM from 'react-dom'

import Inscription from 'container/inscription'

ReactDOM.render(
    <Inscription />,
    document.getElementById('app')
)
