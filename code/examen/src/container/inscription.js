import React, { Component } from 'react'

import Input from 'component/input'
import Select from 'component/select'
import * as file from '../../cities.json'

class Inscription extends Component {
    constructor (props) {
        super(props)

        this.state = {
            cities: file,
            values: {},
            result: {}
        }

        this.handleOnSubmit = this.handleOnSubmit.bind(this)
        this.handleInputOnChange = this.handleInputOnChange.bind(this)
    }

    componentDidMount () {
        fetch('http://localhost:8080/cities', {
            method: 'GET'
        }).then(response => response.json())
            .then((cities) => this.setState({ cities: cities }))
    }

    handleInputOnChange (event) {
        this.setState({
            values: Object.assign(this.state.values, { [event.target.name]: event.target.value })
        })
    }

    handleOnSubmit (event) {
        event.preventDefault()

        fetch('http://localhost:8080/inscriptions', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state.values)
        }).then(response => response.json())
            .then((result) => this.setState({ result: result }))
    }

    render () {
        return (
            <div className='container'>
                <h1>Inscription</h1>

                <form onSubmit={this.handleOnSubmit}>

                    <Input id='firstName_id' label='firstName' type='text' onChange={this.handleInputOnChange} />
                    <Input id='lastName_id' label='lastName' type='text' onChange={this.handleInputOnChange} />
                    <Input id='address_id' label='address' type='text' onChange={this.handleInputOnChange} />
                    <Select id='city_id' label='city' options={JSON.stringify(this.state.cities.default)} onChange={this.handleInputOnChange} />
                    {/* {this.state.cities.default.map((item) => console.log(item.city))} */}

                    <div className='row'>
                        <div className='col text-center'>
                            <input type='submit' value='Sauvegarder' />
                        </div>
                    </div>
                </form>

                <h1>Les valeurs saisies:</h1>

                <pre>
                    {JSON.stringify(this.state.values)}
                </pre>

                <h1>La réponse du serveur:</h1>

                <pre>
                    {JSON.stringify(this.state.result)}
                </pre>
            </div>
        )
    }
}

export default Inscription
