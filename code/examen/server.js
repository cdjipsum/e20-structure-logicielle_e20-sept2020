'use strict'
const express = require('express')
const bodyParser = require('body-parser')
const fs = require('fs')
const CITIES = require('./cities.json')
const app = express()

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse application/json
app.use(bodyParser.json())

// CORS for development
// https://enable-cors.org/server_expressjs.html
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next()
})

const PORT = 8080
const CONTENT_TYPE_JSON = 'application/json'
const HTTP_OK = 200

app.get('/cities', function (request, response) {
    const jsonResponse = JSON.stringify(CITIES, null, 2)
    fs.writeFileSync('result.json', jsonResponse)
    writeResponse(response, jsonResponse)
})

app.post('/cities', function (request, response) {
    const jsonResponse = JSON.stringify(CITIES, null, 2)
    fs.writeFileSync('result.json', jsonResponse)
    writeResponse(response, jsonResponse)
})

app.get('/inscriptions', function (request, response) {
    const jsonResponse = JSON.stringify(request.body, null, 2)
    fs.writeFileSync('result.json', jsonResponse)
    writeResponse(response, jsonResponse)
})

app.post('/inscriptions', function (request, response) {
    const jsonResponse = JSON.stringify(request.body, null, 2)
    fs.writeFileSync('result.json', jsonResponse)
    writeResponse(response, jsonResponse)
})

function writeResponse (response, result) {
    response.writeHead(HTTP_OK, {
        'Content-Type': CONTENT_TYPE_JSON
    })
    response.end(result)
}

app.listen(PORT, function () {
    console.log('Server listening on: http://localhost:%s', PORT)
})
