import React, { Component } from 'react'
import SelectGroupComponent from 'component/selectgroup-component'

const OPTIONS = [{
    label: 'Theropods',
    options: [{
        label: 'Tyrannosaurus',
        number: 0
    }, {
        label: 'Velociraptor',
        number: 1
    }, {
        label: 'Deinonychus',
        number: 2
    }]
}, {
    label: 'Sauropods',
    options: [{
        label: 'Diplodocus',
        number: 3
    }, {
        label: 'Saltasaurus',
        number: 4
    }, {
        label: 'Apatosaurus',
        number: 5
    }]
}]

class FormContainer extends Component {
    render () {
        return (
            <div>
                <form id='form-test'>
                    <SelectGroupComponent text='choose a dinosaur:' id='selectgroud_id' options={OPTIONS} />
                </form>
            </div>
        )
    }
}

export default FormContainer
