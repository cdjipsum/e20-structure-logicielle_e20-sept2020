import * as React from 'react'
import * as ReactDOM from 'react-dom'

import FormContainer from 'container/form-container'

ReactDOM.render(
    <FormContainer />,
    document.getElementById('app')
)
