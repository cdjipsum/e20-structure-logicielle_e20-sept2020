import React from 'react'

function selectOption (value, options) {
    const html = options.map(obj => {
        let rObj = {}
        value.number = obj.number
        rObj = <option value={value.number}>{obj.label}</option>
        return rObj
    })
    return html
}

function optionGroup (value, options) {
    const html = <optgroup label={value}>{selectOption(options, options)}</optgroup>
    return html
}

const SelectGroupComponent = ({ text, id, options }) => (
    <div>
        <label htmlFor={id}>{text}</label>
        <select id={id}>
            {optionGroup(options[0].label, options[0].options)}
            {optionGroup(options[1].label, options[1].options)}
        </select>
    </div>
)

export default SelectGroupComponent
