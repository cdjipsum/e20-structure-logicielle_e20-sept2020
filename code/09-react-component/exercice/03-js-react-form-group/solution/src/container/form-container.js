import React, { Component } from 'react'

import InputComponent from 'component/input-component'
import SelectGroupComponent from 'component/select-group-component'

const OPTIONS = [{
    label: 'Theropods',
    options: [{
        label: 'Tyrannosaurus',
        number: 0
    }, {
        label: 'Velociraptor',
        number: 1
    }, {
        label: 'Deinonychus',
        number: 2
    }]
}, {
    label: 'Sauropods',
    options: [{
        label: 'Diplodocus',
        number: 3
    }, {
        label: 'Saltasaurus',
        number: 4
    }, {
        label: 'Apatosaurus',
        number: 5
    }]
}]

class FormContainer extends Component {
    render () {
        return (
            <div>
                <h1>Formulaire pour l&#39;ajout des usagers</h1>
                <form id='form-test'>
                    <InputComponent
                        text="Nom d'usager:"
                        type='text'
                        id='userName_id'
                        name='userName' />

                    <SelectGroupComponent
                        text='Choose a dinosaur:'
                        id='dinosaur_id'
                        options={OPTIONS} />
                </form>
            </div>
        )
    }
}

export default FormContainer
