import React from 'react'

const buildGroup = (group, index) => {
    return (
        <optgroup label={group.label} key={index}>
            {group.options.map((option, index) => <option value={option.value} key={index}>{option.label}</option>)}
        </optgroup>
    )
}

const SelectGroupComponent = ({ text, id, options }) => (
    <div>
        <label htmlFor={id}>{text}</label>
        <select id={id}>
            {options.map((group, index) => buildGroup(group, index))}
        </select>
    </div>
)

export default SelectGroupComponent
