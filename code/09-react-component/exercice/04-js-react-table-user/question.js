/**
 * Exercice - JS React table user
 *
 * Objectifs:
 *  - Création d'un tableau avec la librairie React
 *
 * Consignes:
 *  - Créer les fichier index.js et index.html
 *  - Créer le conteneur table-user-container
 *  - Créer le component table-user-component
 *  - Ajouter le tableau suivant dans table-user-container et le passer en paramêtre a table-user-component
 *  - table-user-component produit un tableau HTML
 */

const { useReducer } = require("react")

const USERS = [{
    firstName: 'Martin',
    lastName: 'Vachon',
    age: 45
}, {
    firstName: 'Julie',
    lastName: 'Turgeon',
    age: 18
}, {
    firstName: 'Mélanie',
    lastName: 'Simard',
    age: 67
}, {
    firstName: 'Pierre',
    lastName: 'Coté',
    age: 34
}, {
    firstName: 'Nadia',
    lastName: 'Beaulieu',
    age: 35
}]

{/* <tr key={index}>
    <td>{USERS.firstName}</td>
    <td>{USERS.lastName}</td>
</tr> */}
