import React from 'react'

function renderTr (user) {
    return (
        <tr>
            <td>{user.firstName}</td>
            <td>{user.lastName}</td>
            <td>{user.age}</td>
        </tr>
    )
}

const TableUserComponent = ({ users }) => (
    <table>
        <tr>
            <th>First name</th>
            <th>Last name</th>
            <th>Age</th>
        </tr>
        {users.map((user, index) => renderTr(user))}
    </table>
)

export default TableUserComponent
