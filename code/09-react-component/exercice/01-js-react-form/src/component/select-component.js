import React from 'react'

function addSelectOption (value, options) {
    const html = options.map(obj => {
        let rObj = {}
        value.value = obj.value
        rObj = <option value={value.value}>{obj.label}</option>
        return rObj
    })
    return html
}

const SelectComponent = ({ text, id, value, options }) => (
    <div>
        <label htmlFor={id}>{text}</label>
        <select id={id}>
            {addSelectOption(value, options)}
        </select>
    </div>
)

export default SelectComponent
