/**
 * Exercice - JS React form
 *
 * Objectifs:
 *  - Création de formulaire avec la librairie React
 *
 * Consignes:
 *  - Copiez le répertoire src projet js-react dans le projet /09-react-component/exercice/01-js-react-form
 *  -
 *  - Crée la composante de présentation SelectComponent pour ajouter le champ (country)
 *      - La composante produit une uneAjoutez 2 champs de type text (firstName et lastName)
 *  - Ajoutez un champ de type number (age) balise html select avec les paramêtres suivant
 *          text - Le texte pour la balise label
 *          id - L'identifiant unique de la composante
 *          value - L'option sélectionné
 *          options - Un array contenant le nom et la valeur de chaque option
 *
 */

// const OPTIONS = [{
//     label: 'Afghanistan',
//     value: '1'
// }, {
//     label: 'Afrique du Sud',
//     value: '2'
// }, {
//     label: 'Albanie',
//     value: '3'
// }, {
//     label: 'Algérie',
//     value: '4'
// }]
