/**
 * Exercice - JS React Form title
 *
 * Consignes:
 *  - Copier le répertoire src du projet 01-js-react-form
 *  - Crée la composante de présentation RadioTitleComponent (title)
 *      - La composante produit une balise HTML fieldset contenant des inputs type radio
 *      avec les paramètres suivants:
 *          - legend - Le texte pour la balise legend
 *          - id - L'identifiant unique de la composante
 *          - name - le nom de la composante
 *          - titles - Un array contenant le label et number de chaque title
 *      - Utililser la collection ci dessous initialise le le paramètre titres
 */

// const TITLES = [{
//     label: 'Duc',
//     number: 0
// }, {
//     label: 'Duchesse',
//     number: 1
// }, {
//     label: 'Marquis',
//     number: 2
// }, {
//     label: 'Marquise',
//     number: 3
// }, {
//     label: 'Comte',
//     number: 4
// }, {
//     label: 'Comtesse',
//     number: 5
// }]
