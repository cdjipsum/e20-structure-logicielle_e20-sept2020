import React, { Component } from 'react'
import RadioTitleComponent from 'component/radio-component'

const TITLES = [{
    label: 'Duc',
    number: 0
}, {
    label: 'Duchesse',
    number: 1
}, {
    label: 'Marquis',
    number: 2
}, {
    label: 'Marquise',
    number: 3
}, {
    label: 'Comte',
    number: 4
}, {
    label: 'Comtesse',
    number: 5
}]

class FormContainer extends Component {
    render () {
        return (
            <div>
                <form id='form-test'>
                    <RadioTitleComponent legend='Titre' id='radio_id' names={TITLES} titles={TITLES} />
                </form>
            </div>
        )
    }
}

export default FormContainer
