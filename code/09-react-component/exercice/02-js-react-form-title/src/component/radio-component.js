import React from 'react'

function addRadioInput (name, titles) {
    const html = titles.map(obj => {
        let rObj = {}
        name.number = obj.number
        rObj = <div><input type='radio' name='radio_input' value={name.number} />{obj.label}</div>
        return rObj
    })
    return html
}

const RadioTitleComponent = ({ legend, id, names, titles }) => (
    <div>
        <fieldset id={id}>
            <legend>{legend}</legend>
            {addRadioInput(names, titles)}
        </fieldset>
    </div>
)

export default RadioTitleComponent
