import React from 'react'

const buildTitle = (title, name, index) => {
    return (
        <div key={index}>
            <input type='radio' id={'title_' + title.number} name={name} value={title.number} />
            <label htmlFor={'title_' + title.number}>{title.label}</label>
        </div>
    )
}

const RadioTitleComponent = ({ legend, id, name, titles }) => (
    <fieldset id={id} name={name}>
        <legend>{legend}</legend>
        <div>
            {titles.map((title, index) => buildTitle(title, name, index))}
        </div>
    </fieldset>
)

export default RadioTitleComponent
