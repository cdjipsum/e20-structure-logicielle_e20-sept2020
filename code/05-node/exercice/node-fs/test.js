const TEST_FILE_NAME = 'test.json'
const TEST_DATA = [
    { id: 100, userName: 'mvachon', age: 12 },
    { id: 101, userName: 'jcote', age: 66 },
    { id: 102, userName: 'pmartineau', age: 99 }
]
const nodeFs = require('.')
const assert = require('assert').strict

// nodeFs.saveDatas(TEST_FILE_NAME, TEST_DATA)
nodeFs.addData(TEST_FILE_NAME, {'id': 30, 'userName': 'chris', 'age': 30})
console.log(nodeFs.readDatas(TEST_FILE_NAME))
console.log(nodeFs.readData(TEST_FILE_NAME, 1000))

console.log(nodeFs.updateData(TEST_FILE_NAME, {'id': 30, 'userName': 'chris', 'age': 30}))

// assert.strictEqual(nodeFs.readData(TEST_FILE_NAME, 100), nodeFs.readData(TEST_FILE_NAME, 100), 'test de la methode readData()')
assert.deepStrictEqual(nodeFs.readData(TEST_FILE_NAME, 100), nodeFs.readData(TEST_FILE_NAME, 100))