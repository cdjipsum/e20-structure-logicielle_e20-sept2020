/**
 * Exercice - node-fs
 *
 * Objectifs:
 *  - Création d'un  module node réutilisable.
 *  - Le module est une librairie permettant d'utiliser le disque pour émuler une base de données
 *  pour la persistance d'une collection d'objets JSON.
 *
 * Consignes:
 *  - Initialiser un module NodeJs
 *  - Ajouter ESLint
 *  - Créer la structure de fichier suivante:
 *      - src/node-fs.js
 *      - index.js
 *      - test.js
 *
 * Implémenter les méthodes suivantes:
 *  - readDatas (fileName)
 *  - readData (fileName, id)
 *  - saveDatas (fileName, datas)
 *  - addData (fileName, data)
 *  - updateData (fileName, data)
 *
 *  - Les méthodes readData et updateData lance une exception si l'index n'existe pas.
 *      - L'exception a le message suivant: 'Element not found'
 *  - Les méthode addData lance une exception si l'objet existe deja
 *      - L'exception a le message suivant: 'Element already exists'
 *
 *  - Ecrire un test pour chaque méthode.
 *  - Ecrire au moin un test pour vérifier la présence d'exception.
 *
 *  - Utiliser les données suivantes pour exécuter le test
 */
const fs = require('fs')

function readDatas (fileName) {
    const datas = fs.readFileSync(fileName)
    return JSON.parse(datas)
}

function saveDatas (fileName, datas) {
    fs.writeFileSync(fileName, JSON.stringify(datas))
}

function addData (fileName, data) {
    const mesDatas = readDatas(fileName)
    for (let i = 0; i < mesDatas.length - 1; i++) {
        if (mesDatas[i].userName === data) {
            console.log('Element already exists')
            break
        }
        if (i === mesDatas.length - 2 && mesDatas[i].userName !== data) {
            mesDatas.push(data)
            fs.writeFileSync(fileName, JSON.stringify(mesDatas))
            break
        }
    }
}

function readData (fileName, id) {
    const mesDatas = readDatas(fileName)
    const filterResult = mesDatas.filter(function (item) {
        return item.id == id
    })
    return (filterResult == false ? 'Element not found' : filterResult)
}

function updateData (fileName, data) {
    const mesDatas = readDatas(fileName)
    const datas1 = mesDatas.filter(obj => {
        return !(obj.id == data.id)
    });
    const datas2 = mesDatas.filter(obj => {
        return (obj.id == data.id)
    });
    saveDatas(fileName, datas1)
    return (datas2 == false ? 'Element not found' : datas1)
}

module.exports = {
    readDatas,
    readData,
    saveDatas,
    addData,
    updateData
}