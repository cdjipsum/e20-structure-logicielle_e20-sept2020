'use strict'

const { readDatas, readData, saveDatas, addData, updateData } = require('./src/node-fs')

module.exports = {
    readDatas,
    readData,
    saveDatas,
    addData,
    updateData
}
