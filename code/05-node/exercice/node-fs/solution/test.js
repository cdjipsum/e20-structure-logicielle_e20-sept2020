'use strict'

const fs = require('fs')
const assert = require('assert').strict
const nodeFs = require('.')

const TEST_FILE_NAME = 'test.json'

const TEST_DATA = [
    { id: 100, userName: 'mvachon', age: 12 },
    { id: 101, userName: 'jcote', age: 66 },
    { id: 102, userName: 'pmartineau', age: 99 }
]

nodeFs.saveDatas(TEST_FILE_NAME, TEST_DATA)
assert.deepStrictEqual(nodeFs.readDatas(TEST_FILE_NAME), TEST_DATA)

nodeFs.addData(TEST_FILE_NAME, { id: 103, userName: 'chaloupe', age: 32 })

nodeFs.updateData(TEST_FILE_NAME, { id: 103, userName: 'chaloupe', age: 22 })

assert.deepStrictEqual(nodeFs.readData(TEST_FILE_NAME, 103), { id: 103, userName: 'chaloupe', age: 22 })

assert.throws(
    function () {
        nodeFs.updateData(TEST_FILE_NAME, { id: 666, userName: 'chaloupe', age: 22 })
    },
    Error
)

fs.unlinkSync(TEST_FILE_NAME)
