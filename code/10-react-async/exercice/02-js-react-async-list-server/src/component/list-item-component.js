import React from 'react'

import ListContainer from '../container/list-container'

const event = function (e) {
    const click = new ListContainer()
    return click.handleClicks(e)
}

const ListItemComponent = ({ text, indexs }) => (
    // <li onClick={new ListContainer().handleThisClick()}>{text}</li>
    // <li onClick={e => new ListContainer().handleEventClick(e)}>{text}</li>
    <li onClick={event} data-key={indexs}>{text}</li>
)

export default ListItemComponent
