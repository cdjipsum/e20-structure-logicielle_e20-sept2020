/* eslint-disable no-return-assign */
import React, { Component } from 'react'

import ListItemComponent from '../component/list-item-component'

class ListContainer extends Component {
    constructor () {
        super()

        this.state = {
            users: []
        }
        this.nombreUsagers = 0
        this.userName = ''
    }

    componentDidMount () {
        fetch('user-list.json', { method: 'GET' })
            .then(response => response.json())
            .then(response => {
                this.setState({ users: response })
            })
    }

    handleClicks (e) {
        console.log('this represents: ', this)
        console.log('event target is: ', e.currentTarget, 'event is: ', e)
        console.log('index is: ', e.currentTarget.getAttribute('data-key'))
    }

    render () {
        return (
            <div>
                <h1>Liste d&#39;usager</h1>
                <ul>
                    {this.state.users.map((user, index) => <ListItemComponent text={this.userName = user.userName} key={this.nombreUsagers = index} indexs={index} />)}
                </ul>
                <h1>Total d'usager de la liste</h1>
                {this.nombreUsagers = this.userName === '' ? 0 : this.nombreUsagers + 1}
                <h1>Un message alternatif</h1>
                {this.nombreUsagers = this.nombreUsagers > 0 ? 'Il y\'a des usagers dans la liste' : 'Aucun usager trouvé'}
            </div>
        )
    }
}

export default ListContainer
