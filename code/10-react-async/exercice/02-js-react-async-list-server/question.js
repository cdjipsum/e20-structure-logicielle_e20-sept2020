/**
 * Exercice - JS React async list server
 *
 * Consignes:
 *  - Copier le répertoire src du projet 01-js-react-async-list
 *  - Ajouter dans le projet un server NodeJs qui vas retourner la meme structure
 *  json a la URL suivante /users pour remplacer le fichier sur le disque.
 *      - Configurer le server pour présenter le projet react avec le middleware suivant:
 *      app.use(express.static('dist'))
 *          - Le serveur vas donc exposer la url /users et tout le contenu du répertoire /dist.
 *          - Donc les URL suivante seront disponible apres le démarrage du serveur.
 *          http://localhost:8080/index.html
 *          http://localhost:8080/users
 *
 */
