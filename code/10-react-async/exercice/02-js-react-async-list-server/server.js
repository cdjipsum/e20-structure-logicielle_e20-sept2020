'use strcit'

const express = require('express')
const bodyParser = require('body-parser')
// const nodeFs = require('../../../05-node/exercice/node-fs')
const app = express()

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
// parse application/json
app.use(bodyParser.json())
app.use(express.static('dist'))

const PORT = 8080
const HTTP_OK = 200
const CONTENT_TYPE_JSON = 'application/json'
// const CONTENT_TYPE_HTML = 'text/html'
// const fileName = 'user-list.json'
const USERS = [
    {
        userName: 'mvachon'
    },
    {
        userName: 'patate'
    },
    {
        userName: 'gcote'
    },
    {
        userName: 'fmartineau'
    },
    {
        userName: 'mstpierre'
    },
    {
        userName: 'msimard'
    },
    {
        userName: 'agermain'
    }
]

app.get('/users', function (request, response) {
    const datas = JSON.stringify(USERS, null, 4)
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
    response.end(datas)
})

// app.use(function (req, res, next) {
//     res.header('Access-Control-Allow-Origin', '*')
//     res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
//     next()
// })

// app.get('/index.html', function (request, response) {
//     const datas = JSON.stringify(nodeFs.readDatas(fileName2), null, 4)
//     response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_HTML })
//     response.end(datas)
// })

app.listen(PORT, function () {
    console.log('Server listening on: http://localhost:%s', PORT)
})
