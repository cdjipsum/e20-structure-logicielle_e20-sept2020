/**
 * Exercice - JS React async list
 *
 * Consignes:
 *  - Copier le répertoire src du projet js-react-async
 *  - Afficher le total d'usager de la liste
 *  - Affichez un message alternatif lorsque la liste est vide
 *      - Aucun usager trouvé ...
 *  - Ajoutez les 3 console.log() suivant lorsque la liste est cliquée
 *      - this qui représente la classe ListContainer
 *      - index qui représente l'index du li dans le ul
 *      - event qui représente le click event
 *
 */
