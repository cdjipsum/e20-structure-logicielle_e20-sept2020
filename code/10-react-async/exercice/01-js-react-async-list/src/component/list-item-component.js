import React from 'react'

import ListContainer from '../container/list-container'

const event = function (e) {
    const click = new ListContainer()
    return click.handleClicks(e)
}

// const event = (e) => {
//     const click = new ListContainer()
//     return click.handleClicks(e)
// }

const ListItemComponent = () => (
    // <li onClick={new ListContainer().handleThisClick()}>{text}</li>
    // <li onClick={e => new ListContainer().handleEventClick(e)}>{text}</li>
    // <li onClick={event} data-key={indexs}>{text}</li>
    <div>
        <div className=''>
            <div>
                Test 1
                <div class='card-body'>
                    <img class='card-img-top' src='...' alt='Card image cap' />
                    <button onClick={event} data-key='master_id' type='button'>Details</button>
                    Test 2
                </div>
            </div>
        </div>

        {/* <div class='card' style='width: 18rem;'>
            <img class='card-img-top' src='...' alt='Card image cap' />
            <div class='card-body'>
                <h5 class='card-title'>Card title</h5>
                <p class='card-text'>Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                <button onClick={event} data-key='master_id' type='button' class='btn btn-primary'>Details</button>
            </div>
        </div> */}
    </div>
)

export default ListItemComponent
