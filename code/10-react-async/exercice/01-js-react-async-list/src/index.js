import React from 'react'
import ReactDOM from 'react-dom'

import ListContainer from 'container/list-container'

ReactDOM.render(
    <ListContainer />,
    document.getElementById('app')
)
