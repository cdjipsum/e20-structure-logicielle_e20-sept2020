import React from 'react'

function renderButton (button, index) {
    return (
        <span key={index}>
            <button onClick={button.handleOnClick}>{button.label}</button>
        </span>
    )
}

const ToolbarComponent = ({ buttons }) => (
    <div>
        {buttons.map((button, index) => renderButton(button, index))}
    </div>
)

export default ToolbarComponent
