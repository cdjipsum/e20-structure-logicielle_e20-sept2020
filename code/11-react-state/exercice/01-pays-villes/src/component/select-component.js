import React from 'react'

function renderOption (option, index, onChange, value) {
    return <option value={option} key={index} onChange={onChange}>{option}</option>
    // <option value={option[index]} key={index} selected={value === option[index]}>{option[index]}</option>
}

const SelectComponent = ({ text, id, name, value, onChange, options }) => (
    <div>
        <label htmlFor={id}>{text}</label>
        <select class='custom-select' name={name} id={id}>
            {value === 'default' ? Object.keys(options).map((option, index) => renderOption(option, index, onChange, value)) : Object.values(options.Cameroon).map((option, index) => renderOption(option, index, onChange, value))}
        </select>
    </div>
)

export default SelectComponent
