import React, { Component } from 'react'

import * as countries from '../../all-countries-and-cities-json-master/countries.min.json'

class FormContainer extends Component {
    constructor (props) {
        super(props)

        this.state = {
            countries: countries.default,
            selectedCountry: '',
            weather: ''
        }

        this.handleCountriesOnChange = this.handleCountriesOnChange.bind(this)
        this.handleCitiesOnChange = this.handleCitiesOnChange.bind(this)
    }

    handleCountriesOnChange (event) {
        this.setState({
            selectedCountry: event.target.value
        })
    }

    handleCitiesOnChange (event) {
        // Remplace les espaces par un +
        const ville = (event.target.value).replace(/ /gi, '+')

        const URL = 'http://api.openweathermap.org/data/2.5/weather?q=' + ville + '&APPID=52261c83c6e8a4c8e14e163120944701'

        fetch(URL)
            .then(response => response.json())
            .then(meteo => {
                this.setState({ weather: Math.round(meteo.main.temp - 273.15) })
            })
    }

    buildCitiesOptions () {
        let cities = []

        if (this.state.selectedCountry !== '') {
            cities = this.state.countries[this.state.selectedCountry]
        }

        return cities.map((city, index) => <option value={city} key={index}>{city}</option>)
    }

    render () {
        return (
            <div className='container'>

                <h1>Country / City</h1>

                <form>
                    <div className='row'>
                        <div className='col form-group'>
                            <label htmlFor='country_id'>Country</label>
                            <select className='custom-select' id='country_id' onChange={this.handleCountriesOnChange}>
                                {Object.keys(this.state.countries).map((country, index) => <option key={index} value={country}>{country}</option>)}
                            </select>
                        </div>
                        <div className='col form-group'>
                            <label htmlFor='city_id'>City</label>
                            <select className='custom-select' id='city_id' onChange={this.handleCitiesOnChange}>
                                {this.buildCitiesOptions()}
                            </select>
                        </div>
                    </div>
                </form>

                <h1>openweathermap.org result</h1>

                <div>
                    <pre>
                        {this.state.weather} &#8451;
                    </pre>
                </div>
            </div>
        )
    }
}

export default FormContainer
