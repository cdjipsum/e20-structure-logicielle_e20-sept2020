/**
 * Exercice - In-place editor
 *
 *
 * Consignes:
 *  - Créer une composante InputComponent et une composante TextComponent pour produire un
 *  input et un textarea
 *  - Créer le conteneur FormContainer pour permettre l'édition du state si-dessous
 *  - Chaque champ du formulaire devient éditable en cliquant sur la valeur.
 *  - L'orsqu'un champ devient éditable, tout les autres reviennent en lecture seul.
 *  - Sur un click du bouton soumettre, le fomulaire reviens dans l'état initial (lecture seul).
 *  - Note: ne pas traiter la soumission dans cet exercice.
 *
 */

this.state = {
    userNameIsEditing: false,
    userNameValue: 'mvachon',

    emailIsEditing: false,
    emailValue: 'mvachon@server.com',

    messageIsEditing: false,
    messageValue: 'message de test'
}
