import React from 'react'

const InputComponent = ({ text, type, id, value, onChange, onClick, isEditing }) => (
    <div onClick={onClick}>
        <label htmlFor={id}>{text}</label>
        <span>
            {isEditing ? <input type={type} id={id} value={value} onChange={onChange} /> : value}
        </span>
    </div>
)

export default InputComponent
