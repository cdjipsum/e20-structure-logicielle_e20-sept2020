import React from 'react'

const TextComponent = ({ text, id, value, onChange, onClick, isEditing }) => (
    <div onClick={onClick}>
        <label htmlFor={id}>{text}</label>
        <span>
            {isEditing ? <textarea id={id} value={value} onChange={onChange} /> : value}
        </span>
    </div>
)

export default TextComponent
