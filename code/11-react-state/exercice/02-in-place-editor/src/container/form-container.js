import React, { Component } from 'react'

import InputComponent from 'component/input-component'
import TextComponent from 'component/text-component'

class FormContainer extends Component {
    constructor (props) {
        super(props)

        this.state = {
            userNameIsEditing: false,
            userNameValue: 'mvachon',

            emailIsEditing: false,
            emailValue: 'mvachon@server.com',

            messageIsEditing: false,
            messageValue: 'message de test'
        }

        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange (event, fieldName) {
        this.setState({
            [fieldName + 'Value']: event.target.value
        })
    }

    clearEditing () {
        this.setState({
            userNameIsEditing: false,
            emailIsEditing: false,
            messageIsEditing: false
        })
    }

    handleClick (fieldName) {
        this.clearEditing()

        this.setState({
            [fieldName + 'IsEditing']: true
        })
    }

    handleSubmit (event) {
        event.preventDefault()
        this.clearEditing()
        console.log(this.state) // eslint-disable-line
    }

    render () {
        return (
            <div>

                <h1>Profil utilisateur</h1>

                <form onSubmit={this.handleSubmit}>
                    <InputComponent
                        text="Nom d'usager:"
                        type='text'
                        id='userName_id'
                        value={this.state.userNameValue}
                        isEditing={this.state.userNameIsEditing}
                        onChange={(event) => this.handleChange(event, 'userName')}
                        onClick={() => this.handleClick('userName')}
                    />

                    <InputComponent
                        text='Email:'
                        type='email'
                        id='email_id'
                        value={this.state.emailValue}
                        isEditing={this.state.emailIsEditing}
                        onChange={(event) => this.handleChange(event, 'email')}
                        onClick={() => this.handleClick('email')}
                    />

                    <TextComponent
                        text='Message:'
                        id='message_id'
                        value={this.state.messageValue}
                        isEditing={this.state.messageIsEditing}
                        onChange={(event) => this.handleChange(event, 'message')}
                        onClick={() => this.handleClick('message')}
                    />

                    <div>
                        <input type='submit' value='Submit' />
                    </div>

                    <h1>State</h1>
                    <pre>
                        {JSON.stringify(this.state, null, 4)}
                    </pre>
                </form>
            </div>
        )
    }
}

export default FormContainer
