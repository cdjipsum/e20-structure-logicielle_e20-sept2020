import React, { Component } from 'react'

import InputComponent from 'component/input-component'

class FormContainer extends Component {
    constructor (props) {
        super(props)

        this.state = {
            formValues: {}
        }

        this.handleInputOnChange = this.handleInputOnChange.bind(this)
    }

    handleInputOnChange (event) {
        this.setState({
            formValues: Object.assign(this.state.formValues, { [event.target.name]: event.target.value })
        })
    }

    render () {
        return (
            <div>
                <h1>Formulaire</h1>
                <form>
                    <InputComponent onChange={this.handleInputOnChange} label="Nom d'usager:" type='text' id='userName' name='userName' />

                    <InputComponent onChange={this.handleInputOnChange} label='Prénom:' type='text' id='firstName' name='firstName' />

                    <InputComponent onChange={this.handleInputOnChange} label='Nom:' type='text' id='lastName' name='lastName' />
                </form>

                <h1>État</h1>
                <pre>
                    {JSON.stringify(this.state, null, 4)}
                </pre>
            </div>
        )
    }
}

export default FormContainer
