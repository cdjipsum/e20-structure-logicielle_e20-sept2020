/**
 * Exercice - mathematic
 *
 * Objectifs:
 *  Création d'un  module node réutilisable.
 *  Le module est une librairie de fonction mathématique.
 *
 * Consignes:
 *  - Initialiser un module Node: mathematic
 *  - Installer les dépendance pour ESLint: eslint, eslint-config-standard, eslint-plugin-import,
 *  eslint-plugin-node, eslint-plugin-promise, eslint-plugin-standard
 *  - Ajouter la structure de fichier suivante:
 *      - /src/mathematic.js
 *      - index.js
 *      - test.js
 *  - Implémenter les 4 méthodes suivantes: add, div, mul, sub
 *  - Utiliser console.log pour exécuter chaque méthode dans le fichier test.js
 *  - Ajouter la configuration nécessaire pour exécuter les tests avec la commande: npm test
 *
 */
