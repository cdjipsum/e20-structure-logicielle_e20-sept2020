function mul (a, b) {
    return (a * b)
}

function sub (a, b) {
    return (a - b)
}

function add (a, b) {
    return (a + b)
}

function div (a, b) {
    return (a / b)
}

module.exports = {
    mul,
    sub,
    add,
    div
}
