/**
 * Exercice - File upload
 *
 * Objectifs:
 *  - Création d'un serveur NodeJs pour supporter le file upload
 *
 * Consignes:
 *  - Initialiser un module NodeJs
 *  - Installer et configurer ESLint
 *  - installer le module suivant: https://www.npmjs.com/package/multer
 *  - Créer le fichier server.js
 *  - Ajouter le traitement de la url POST /upload permettant d'écrire le fichier dans le
 *  répertoire file-upload/upload-folder
 *  - Configurer le serveur express pour servir des fichier static dans le répertoire public/
 *      - https://expressjs.com/en/starter/static-files.html
 *  - Créer le fichier static public/index.html contenant un formulaire HTML permettant le upload d'un fichier.
 *
 */
