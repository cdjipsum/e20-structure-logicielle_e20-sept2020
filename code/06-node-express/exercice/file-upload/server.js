'use strcit'

// console.log('Execution du serveur')
const express = require('express')
const bodyParser = require('body-parser')
const nodeFs = require('../../../05-node/exercice/node-fs')
const multer = require('multer')
const upload = multer({ dest: 'upload-folder/' })
const app = express()

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
// parse application/json
app.use(bodyParser.json())
app.use(express.static('public'))

const PORT = 8080
const HTTP_OK = 200
const CONTENT_TYPE_JSON = 'application/json'
const CONTENT_TYPE_HTML = 'text/html'
const fileName = 'test-json'
const fs = require('fs')
const file = fs.readFileSync('public/index.html')
const TEST_DATA = [
    { id: 100, userName: 'mvachon', age: 12 },
    { id: 101, userName: 'jcote', age: 66 },
    { id: 102, userName: 'pmartineau', age: 99 }
]
nodeFs.saveDatas(fileName, TEST_DATA)

app.get('/upload', upload.single('a_file'), function (request, response) {
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_HTML })
    response.end(file)
})

app.post('/upload', upload.single('a_file'), function (request, response) {
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_HTML })
    response.end(file)
})

app.get('/datas', function (request, response) {
    const datas = JSON.stringify(nodeFs.readDatas(fileName), null, 4)
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
    response.end(datas)
})

app.post('/datas', function (request, response) {
    const datas = JSON.stringify(nodeFs.readDatas(fileName), null, 4)
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
    response.end(datas)
})

app.get('/datas/:a', function (request, response) {
    const data = JSON.stringify(nodeFs.readData(fileName, parseInt(request.params.a)), null, 4)
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_HTML })
    response.end(data)
})

app.put('/datas', function (request, response) {
    nodeFs.addData(fileName, { id: 777, userName: 'bateau2', age: 66 })
    const datas = JSON.stringify(nodeFs.readDatas(fileName), null, 4)
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
    response.end(datas)
})

app.delete('/datas/:a', function (request, response) {
    const datas = JSON.stringify(nodeFs.updateData(fileName, parseInt(request.params.a)), null, 4)
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
    response.end(datas)
})

app.listen(PORT, function () {
    console.log('Server listening on: http://localhost:%s', PORT)
})
