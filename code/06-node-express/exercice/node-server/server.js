'use strcit'

/**
 * Exercice - node-server
 *
 * Objectifs:
 *  - Création d'un serveur NodeJs pour exposer un API au format REST.
 *
 * Consignes:
 *  - Initialiser un module NodeJs
 *  - Installer ESLint
 *  - Créer le fichier server.js
 *  - Ecrire le code pour exposer le projet node-fs en REST
 *  - Note: Ne pas copier le projet, utiliser par exemple: const nodeFs = require('../../05-node/exercice/node-fs')
 *  - Créer le fichier test.rest avec un test URL pour chaque requête
 *
 */

// console.log('Execution du serveur')
const express = require('express')
const bodyParser = require('body-parser')
const nodeFs = require('../../../05-node/exercice/node-fs')
const { Router } = require('express')
const app = express()

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse application/json
app.use(bodyParser.json())

const PORT = 8080
const HTTP_OK = 200
const CONTENT_TYPE_JSON = 'application/json'
const CONTENT_TYPE_HTML = 'text/html'
const fileName = 'test-json'
const TEST_DATA = [
    { id: 100, userName: 'mvachon', age: 12 },
    { id: 101, userName: 'jcote', age: 66 },
    { id: 102, userName: 'pmartineau', age: 99 }
]
nodeFs.saveDatas(fileName, TEST_DATA)

// app.get('/datas', function (request, response) {
//     if (request.method === 'GET') {
//         const datas = JSON.stringify(nodeFs.readDatas(fileName), null, 4)
//         response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
//         response.end(datas)
//     } else if (request.method === 'PUT') {
//         nodeFs.addData(fileName, { id: 777, userName: 'bateau2', age: 66 })
//         const datas = JSON.stringify(nodeFs.readDatas(fileName), null, 4)
//         response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
//         response.end(datas)
//     }
// })

// app.post('/datas', function (request, response) {
//     if (request.method === 'GET') {
//         const datas = JSON.stringify(nodeFs.readDatas(fileName), null, 4)
//         response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
//         response.end(datas)
//     }
// })

// app.get('/datas/:a', function (request, response) {
//     if (request.method === 'GET') {
//         const data = JSON.stringify(nodeFs.readData(fileName, parseInt(request.params.a)), null, 4)
//         response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_HTML })
//         response.end(data)
//     } else if (request.method === 'DELETE') {
//         const datas = JSON.stringify(nodeFs.updateData(fileName, parseInt(request.params.a)), null, 4)
//         response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
//         response.end(datas)
//     }
// })

app.get('/datas', function (request, response) {
    const datas = JSON.stringify(nodeFs.readDatas(fileName), null, 4)
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
    response.end(datas)
})

app.get('/datas/:a', function (request, response) {
    const data = JSON.stringify(nodeFs.readData(fileName, parseInt(request.params.a)), null, 4)
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_HTML })
    response.end(data)
})

app.post('/datas', function (request, response) {
    const datas = JSON.stringify(nodeFs.readDatas(fileName), null, 4)
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
    response.end(datas)
})

app.put('/datas', function (request, response) {
    nodeFs.addData(fileName, { id: 777, userName: 'bateau2', age: 66 })
    const datas = JSON.stringify(nodeFs.readDatas(fileName), null, 4)
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
    response.end(datas)
})

app.delete('/datas/:a', function (request, response) {
    const datas = JSON.stringify(nodeFs.updateData(fileName, parseInt(request.params.a)), null, 4)
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
    response.end(datas)
})

// for test
// app.get('/', function (request, response) {
//     response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_HTML })
//     response.end('<h1>Home page</h1>')
// })

// app.get('/test-param/:a', function (request, response) {
//     response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_HTML })
//     response.end('<h1>' + request.params.a + '</h1>')
// })

// app.get('/test-json', function (request, response) {
//     const testObject = { a: 'val11', b: 'val2' }
//     const testObjectString = JSON.stringify(testObject, null, 4)

//     response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
//     response.end(testObjectString)
// })

// app.post('/test-json', function (request, response) {
//     const testObjectString = JSON.stringify(request.body, null, 4)

//     response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
//     response.end(testObjectString)
// })

app.listen(PORT, function () {
    console.log('Server listening on: http://localhost:%s', PORT)
})
