'use strict'
const express = require('express')
const bodyParser = require('body-parser')

const app = express()

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse application/json
app.use(bodyParser.json())

const nodeFs = require('../../../05-node/exercice/node-fs')

const PORT = 8080
const CONTENT_TYPE_JSON = 'application/json'
const CONTENT_TYPE_HTML = 'text/html'
const HTTP_OK = 200

const DATAS_FILE_NAME = 'test.json'

const TEST_DATA = [
    { id: 100, userName: 'mvachon', age: 12 },
    { id: 101, userName: 'jcote', age: 66 },
    { id: 102, userName: 'pmartineau', age: 99 }
]

nodeFs.saveDatas(DATAS_FILE_NAME, TEST_DATA)

app.get('/', function (request, response) {
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_HTML })
    response.end('<h1>Home page</h1>')
})

app.get('/datas', function (request, response) {
    const datas = nodeFs.readDatas(DATAS_FILE_NAME)
    writeJSONResponse(request, response, datas)
})

app.get('/datas/:id', function (request, response) {
    const data = nodeFs.readData(DATAS_FILE_NAME, request.params.id)
    writeJSONResponse(request, response, data)
})

app.post('/datas', function (request, response) {
    console.log('TYPE', typeof request.body)

    nodeFs.addData(DATAS_FILE_NAME, request.body)
    const datas = nodeFs.readDatas(DATAS_FILE_NAME)
    writeJSONResponse(request, response, datas)
})

app.put('/datas', function (request, response) {
    nodeFs.updateData(DATAS_FILE_NAME, request.body)
    const datas = nodeFs.readDatas(DATAS_FILE_NAME)
    writeJSONResponse(request, response, datas)
})

app.delete('/datas/:id', function (request, response) {
    // La méthode n'est pas implémenté danns nodeFs
    // L'objectif est seulement de démontrer la méthode HTTP delete
    writeJSONResponse(request, response, {})
})

function writeJSONResponse (request, response, result) {
    response.writeHead(HTTP_OK, { 'Content-Type': CONTENT_TYPE_JSON })
    response.end(JSON.stringify(result, null, 2))
}

app.listen(PORT, function () {
    console.log('Server listening on: http://localhost:%s', PORT)
})
