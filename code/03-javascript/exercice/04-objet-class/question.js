/**
 * Exercice - Objet-class
 *
 * Objectifs:
 *  - Révision des concepts de modélisation objet avec JavaScript.
 *
 * Consignes
 *  - Crée le fichier objet-class.js
 *  - Utiliser la syntaxe ES6 pour créer l'objet: TestObjetClass
 *
 * L'objet TestObjetClass doit contenir:
 * Une méthode privé: privateFunction
 * Une méthode statique: staticFunction
 * Une méthode publique: publicFunction
 * Une méthode publique qui retourne le résultat de la méthode privateFunction: executePrivateFunction
 *
 * Utilisez console.log pour afficher le résultat de tout ce qui est publique dans chaque fichier
 *
 */
