/**
 * Exercice - ES6 Array
 *
 * Objectifs:
 *  - Utiliser les fonctions ES6 pour la manipulation des array (map, filter, reduce)
 *
 * Consignes:
 *  - Crée le fichier es6-array.js
 *  - En utilisant la structure de donnée USER_ARRAY, écrire le code pour produire les 3 résultats suivants
 */

const USER_ARRAY = [
    { id: 1, name: 'Martin', age: 45 },
    { id: 2, name: 'Pierre', age: 15 },
    { id: 3, name: 'Josée', age: 14 },
    { id: 4, name: 'Melanie', age: 32 },
    { id: 5, name: 'Sonia', age: 24 }
]

/* Résultat 1 (Retirer la propriété age de tout les objet de la collection) */

    [
        { id: 1, name: 'Martin' },
        { id: 2, name: 'Pierre' },
        { id: 3, name: 'Josée' },
        { id: 4, name: 'Melanie' },
        { id: 5, name: 'Sonia' }
    ]

/* Résultat 2 (Conservez seulement les objets de la collection dont la propriété age est supérieur a 15) */

    [
        { id: 1, name: 'Martin', age: 45 },
        { id: 4, name: 'Melanie', age: 32 },
        { id: 5, name: 'Sonia', age: 24 }
    ]

/* Résultat 3 (Calculer la moyenne d'age de la collection) */

26
