/**
 * Exercice - Objet-function
 *
 * Objectifs:
 *  - Révision des concepts de modélisation objet avec JavaScript.
 *
 * Consignes
 *  - Crée le fichier objet-fonction.js
 *  - Utiliser la syntaxe ES5 pour créer l'objet: ObjetFonction (Ne pas utiliser prototype)
 *
 * L'objet ObjetFonction doit contenir:
 *  Une variable privé: privateVariable
 *  Une variable publique: publicVariable
 *  Une méthode privé: privateFunction
 *  Une méthode publique qui retourne la valeur de la variable privateVariable: getPrivateVariable
 *  Une méthode publique qui retourne la valeur produite par la méthode privateFunction: executePrivateFunction
 *
 * Utilisez console.log pour afficher le résultat de tout ce qui est publique dans chaque fichier
 *
 */

class ObjetFonction {
    constructor () {
        this.publicVariable = 'public data'
        function privateFunction () {
            return 'private variable 1'
        }
        this.privateVariable = privateFunction()
    }

    getPrivateVariable () {
        this.privateVariable = 'private variable 2'
        return this.privateVariable
    }

    executePrivateFunction () {
        return this.privateVariable
    }
}
const test = new ObjetFonction()

console.log(test.executePrivateFunction(), ' - ', test.getPrivateVariable(), ' - ', test.publicVariable)
