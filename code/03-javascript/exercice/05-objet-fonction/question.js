/**
 * Exercice - Objet-function
 *
 * Objectifs:
 *  - Révision des concepts de modélisation objet avec JavaScript.
 *
 * Consignes
 *  - Crée le fichier objet-fonction.js
 *  - Utiliser la syntaxe ES5 pour créer l'objet: ObjetFonction (Ne pas utiliser prototype)
 *
 * L'objet ObjetFonction doit contenir:
 *  Une variable privé: privateVariable
 *  Une variable publique: publicVariable
 *  Une méthode privé: privateFunction
 *  Une méthode publique qui retourne la valeur de la variable privateVariable: getPrivateVariable
 *  Une méthode publique qui retourne la valeur produite par la méthode privateFunction: executePrivateFunction
 *
 * Utilisez console.log pour afficher le résultat de tout ce qui est publique dans chaque fichier
 *
 */
